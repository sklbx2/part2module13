// Fill out your copyright notice in the Description page of Project Settings.


#include "StaticMeshLODHelper.h"

#include "StaticMeshEditorSubsystem.h"			// "StaticMeshEditor" module in Plugin build.cs
#include "StaticMeshEditorSubsystemHelpers.h"	// "StaticMeshEditor" module in Plugin build.cs
#include "UnrealEdGlobals.h"					// "UnrealEd" module in Plugin build.cs

TArray<UStaticMesh*> UStaticMeshLODHelper::GenerateLOD(TArray<UObject*> StaticMeshes, const FStaticMeshReductionOptions& ReductionOptions, bool& bOutSuccess, FString& OutInfoMessage)
{
	// Make sure there are StaticMeshes to Generate LOD
	if (StaticMeshes.IsEmpty())
	{
		bOutSuccess = false;
		OutInfoMessage = FString::Printf(TEXT("Generate LOD FAILED - StaticMeshes array is empty"));
		return TArray<UStaticMesh*>();
	}

	// Get Subsystem
	auto* StaticMeshEditorSubsystem = GEditor->GetEditorSubsystem<UStaticMeshEditorSubsystem>();
	if (!StaticMeshEditorSubsystem)
	{
		bOutSuccess = false;
		OutInfoMessage = FString::Printf(TEXT("Generate LOD FAILED - StaticMeshEditorSubsystem is not valid"));
		return TArray<UStaticMesh*>();
	}

	bOutSuccess = false;
	TArray<UStaticMesh*> ResultArray;
	for (auto& Obj : StaticMeshes)
	{
		if (auto* Mesh = Cast<UStaticMesh>(Obj))
		{
			if (StaticMeshEditorSubsystem->SetLods(Mesh, ReductionOptions) != -1)
			{
				bOutSuccess = true;
				ResultArray.Add(Mesh);
			}
		}
	}

	if (!bOutSuccess)
	{
		OutInfoMessage = FString::Printf(TEXT("Generate LOD FAILED - Set Lods failed"));
		return TArray<UStaticMesh*>();
	}

	OutInfoMessage = FString::Printf(TEXT("Generate LOD SUCCESS"));
	return ResultArray;
}

TArray<UStaticMesh*> UStaticMeshLODHelper::ClearLOD(TArray<UObject*> StaticMeshes, bool& bOutSuccess, FString& OutInfoMessage)
{
	// Make sure there are StaticMeshes to Generate LOD
	if (StaticMeshes.IsEmpty())
	{
		bOutSuccess = false;
		OutInfoMessage = FString::Printf(TEXT("ClearLOD LOD FAILED - StaticMeshes array is empty"));
		return TArray<UStaticMesh*>();
	}

	// Get Subsystem
	auto* StaticMeshEditorSubsystem = GEditor->GetEditorSubsystem<UStaticMeshEditorSubsystem>();
	if (!StaticMeshEditorSubsystem)
	{
		bOutSuccess = false;
		OutInfoMessage = FString::Printf(TEXT("ClearLOD LOD FAILED - StaticMeshEditorSubsystem is not valid"));
		return TArray<UStaticMesh*>();
	}

	// Creating ReductionSettings with one default LOD
	TArray<FStaticMeshReductionSettings> ReductionSettings;
	FStaticMeshReductionSettings ZeroSettings;
	ReductionSettings.Add(ZeroSettings);
	FStaticMeshReductionOptions Options;
	Options.ReductionSettings = ReductionSettings;

	bOutSuccess = false;
	TArray<UStaticMesh*> ResultArray;
	for (auto& Obj : StaticMeshes)
	{
		if (auto* Mesh = Cast<UStaticMesh>(Obj))
		{
			if (StaticMeshEditorSubsystem->SetLods(Mesh, Options) != -1)
			{
				bOutSuccess = true;
				ResultArray.Add(Mesh);
			}
		}
	}

	if (!bOutSuccess)
	{
		OutInfoMessage = FString::Printf(TEXT("Clear LOD FAILED - Set Lods failed"));
		return TArray<UStaticMesh*>();
	}

	OutInfoMessage = FString::Printf(TEXT("Clear LOD SUCCESS"));
	return ResultArray;
}
