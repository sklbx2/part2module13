// Fill out your copyright notice in the Description page of Project Settings.


#include "ModifyAssetsCommandlet.h"
#include "Engine/StaticMesh.h"

#include "Modules/ModuleManager.h"
#include "AssetRegistry/AssetRegistryModule.h"

#include "StaticMeshEditorSubsystem.h"			// "StaticMeshEditor" module in Plugin build.cs
#include "StaticMeshEditorSubsystemHelpers.h"	// "StaticMeshEditor" module in Plugin build.cs
//#include "UnrealEdGlobals.h"					// "UnrealEd" module in Plugin build.cs

#include "UObject/SavePackage.h"

inline int32 UModifyAssetsCommandlet::Main(const FString& Params)
{

	TArray<FString> Tokens;		// "token1"
	TArray<FString> Switches;	// "-switch1"

	//TMap<FString, FString> ParamsMap;	// "-key=value"

	ParseCommandLine(*Params, Tokens, Switches/*, ParamsMap*/);
	//  Example:
	//  -run=ModifyAssets -GenerateLod -Debug /Game/EditorExtension/NewMesh /Game/EditorExtension/NewMesh1

	// Run shortcut:
	// "C:\Program Files\Epic Games\UE_5.3\Engine\Binaries\Win64\UnrealEditor.exe" "C:\%ProjectPath%\EditorExtension\EditorExtension.uproject" -run=ModifyAssets -GenerateLod /Game/%Folder1 Path% /Game/%Folder2 Path%

	if (Switches.Contains(TEXT("GenerateLod")))
	{
		if (Tokens.Num()>0)
		{
			ProccesAssets(Tokens);
		}
	}

	return 0;
}

void UModifyAssetsCommandlet::ProccesAssets(TArray<FString> RootDirectories)
{
	// Dynamically load the asset registration module
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(AssetRegistryConstants::ModuleName);
	
	AssetRegistryModule.Get().SearchAllAssets(true);

	TArray<FAssetData> AssetList;

	FTopLevelAssetPath TopLevelClassName(UStaticMesh::StaticClass()->GetClassPathName());
	AssetRegistryModule.Get().GetAssetsByClass(TopLevelClassName, AssetList, true);

	for (auto& AssetData : AssetList)
	{
		// Check that the selected asset is in one of the directories that we passed
		for (auto& RootDirectory : RootDirectories)
		{
			if (AssetData.GetSoftObjectPath().ToString().StartsWith(RootDirectory, ESearchCase::IgnoreCase))
			{
				UObject* AssetInstance = AssetData.GetAsset();

				ModifyLod(AssetInstance);

				SaveAsset(AssetInstance);

				break;
			}
		}
	}
}

void UModifyAssetsCommandlet::ModifyLod(UObject* AssetInstance)
{
	if (auto* Mesh = Cast<UStaticMesh>(AssetInstance))
	{
		TArray<FStaticMeshReductionSettings> ReductionSettings;
		FStaticMeshReductionSettings TempSettings;

		// LOD 0
		TempSettings.PercentTriangles = 1;
		TempSettings.ScreenSize = 0.9;
		ReductionSettings.Add(TempSettings);

		// LOD 1
		TempSettings.PercentTriangles = 0.5;
		TempSettings.ScreenSize = 0.5;
		ReductionSettings.Add(TempSettings);

		// LOD 2
		TempSettings.PercentTriangles = 0.1;
		TempSettings.ScreenSize = 0.3;
		ReductionSettings.Add(TempSettings);

		FStaticMeshReductionOptions Options;
		Options.ReductionSettings = ReductionSettings;

		// Manually setting Lods because we can't get StaticMeshEditorSubsystem in comandlet because we do not have Editor
		int LodResult = SetLods(Mesh, Options);

		// Marking the .uasset file of our asset as changed
		bool result = AssetInstance->MarkPackageDirty();
	}	
}

void UModifyAssetsCommandlet::SaveAsset(UObject* AssetInstance)
{
	if (AssetInstance)
	{		
		// Get the .uasset file
		if (UPackage* Package = AssetInstance->GetPackage())
		{
			// Check that the file is marked modified
			if (Package->IsDirty())
			{			
				// Get the full path of the file
				FString PackageName = FPackageName::LongPackageNameToFilename(Package->GetPathName(), 
					FPackageName::GetAssetPackageExtension());

				UE_LOG(LogClass, Log, TEXT("Saving asset to: %s..."), *PackageName);

				FSavePackageArgs SaveArgs;
				SaveArgs.TopLevelFlags = RF_Standalone;
				SaveArgs.Error = GLog;

				// Saving file
				if (Package->SavePackage(Package, AssetInstance, *PackageName, SaveArgs))
				{	
					UE_LOG(LogClass, Log, TEXT("Done."));
				}
				else
				{	
					UE_LOG(LogClass, Log, TEXT("Can't save asset!"));
				}
			}
		}
	}
}

int32 UModifyAssetsCommandlet::SetLods(UStaticMesh* StaticMesh, const FStaticMeshReductionOptions& ReductionOptions)
{
	bool bApplyChanges = true;

	// Copy of code from UStaticMeshEditorSubsystem
	// Because without Editor it is impossible to get StaticMeshEditorSubsystem to call this function as a class method
	// Sections concerning the Editor have been removed

	TGuardValue<bool> UnattendedScriptGuard(GIsRunningUnattendedScript, true);

	if (StaticMesh == nullptr)
	{
		UE_LOG(LogClass, Error, TEXT("SetLODs: The StaticMesh is null."));
		return -1;
	}

	// If LOD 0 does not exist, warn and return
	if (StaticMesh->GetNumSourceModels() == 0)
	{
		UE_LOG(LogClass, Error, TEXT("SetLODs: This StaticMesh does not have LOD 0."));
		return -1;
	}

	if (ReductionOptions.ReductionSettings.Num() == 0)
	{
		UE_LOG(LogClass, Error, TEXT("SetLODs: Nothing done as no LOD settings were provided."));
		return -1;
	}

	if (bApplyChanges)
	{
		StaticMesh->Modify();
	}

	// Resize array of LODs to only keep LOD 0
	StaticMesh->SetNumSourceModels(1);

	// Set up LOD 0
	StaticMesh->GetSourceModel(0).ReductionSettings.PercentTriangles = ReductionOptions.ReductionSettings[0].PercentTriangles;
	StaticMesh->GetSourceModel(0).ScreenSize = ReductionOptions.ReductionSettings[0].ScreenSize;

	int32 LODIndex = 1;
	for (; LODIndex < ReductionOptions.ReductionSettings.Num(); ++LODIndex)
	{
		// Create new SourceModel for new LOD
		FStaticMeshSourceModel& SrcModel = StaticMesh->AddSourceModel();

		// Copy settings from previous LOD
		SrcModel.BuildSettings = StaticMesh->GetSourceModel(LODIndex - 1).BuildSettings;
		SrcModel.ReductionSettings = StaticMesh->GetSourceModel(LODIndex - 1).ReductionSettings;

		// Modify reduction settings based on user's requirements
		SrcModel.ReductionSettings.PercentTriangles = ReductionOptions.ReductionSettings[LODIndex].PercentTriangles;
		SrcModel.ScreenSize = ReductionOptions.ReductionSettings[LODIndex].ScreenSize;

		// Stop when reaching maximum of supported LODs
		if (StaticMesh->GetNumSourceModels() == MAX_STATIC_MESH_LODS)
		{
			break;
		}
	}

	StaticMesh->bAutoComputeLODScreenSize = ReductionOptions.bAutoComputeLODScreenSize ? 1 : 0;

	if (bApplyChanges)
	{
		// Request re-building of mesh with new LODs
		StaticMesh->PostEditChange();
	}

	return LODIndex;
}
