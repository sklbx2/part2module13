// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "StaticMeshLODHelper.generated.h"

class UStaticMesh;
struct FStaticMeshReductionOptions;

/**
 * 
 */
UCLASS()
class SKILLBOXPLUGIN_API UStaticMeshLODHelper : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	/**
	* Generate LODs for given StaticMeshes
	* 
	* @param StaticMeshes		The meshes to generate LOD
	* @param ReductionOptions	Options on how to generate LODs on the meshes.
	* @param bOutSuccess		If the action was a success or not
	* @param OutInfoMessage		MoreInfomation about the action's result
	* 
	* @return Array of meshes with Generated LODs
	*/
	UFUNCTION(BlueprintCallable, Category = "StaticMeshHelper Plugin")
	static TArray<UStaticMesh*> GenerateLOD(TArray<UObject*> StaticMeshes, const FStaticMeshReductionOptions& ReductionOptions, bool& bOutSuccess, FString& OutInfoMessage);

	/**
	* Clear LODs for given StaticMeshes
	*
	* @param StaticMeshes		The meshes to clear LOD
	* @param bOutSuccess		If the action was a success or not
	* @param OutInfoMessage		MoreInfomation about the action's result
	*
	* @return Array of meshes with Cleared LODs
	*/
	UFUNCTION(BlueprintCallable, Category = "StaticMeshHelper Plugin")
	static TArray<UStaticMesh*> ClearLOD(TArray<UObject*> StaticMeshes, bool& bOutSuccess, FString& OutInfoMessage);
};
